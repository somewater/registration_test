class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.integer   :user_id, null: false
      t.string    :email, null: false
      t.datetime  :last_invitation_at
      t.string    :confirmation_token

      t.timestamps null: false
    end

    add_index :invites, [:user_id, :email], unique: true
  end
end
