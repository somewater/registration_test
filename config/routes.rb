Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: "users/registrations" }

  root 'welcome#index', as: 'root'

  resource :invites
end
