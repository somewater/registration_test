require 'rails_helper'

describe InvitesController do
  let(:user) { create(:user) }
  let(:mail) { InviteMailer.invite_friend(user, ) }

  before(:each) do
    allow(request.env['warden']).to receive(:authenticate!).and_return(user)
    allow(controller).to receive(:current_user).and_return(user)
  end

  after(:each) do
    ActionMailer::Base.deliveries.clear
  end

  def advance_invite_time!
    Invite.find_by(email: "friend@gmail.com", user_id: user.id).
        update_column(:last_invitation_at, Rails.configuration.application_config['invites']['day_timeout'].days.ago)
  end

  it "registered user can send invites" do
    expect do
      post :create, {invite: {email: "friend@gmail.com" }}
    end.to change{Invite.count}.by(1)

    expect(ActionMailer::Base.deliveries.count).to eq(1)

    expect(flash[:notice]).to be_present
  end

  it "registered user can't send invites more than once in N days" do
    post :create, {invite: {email: "friend@gmail.com" }}
    expect(ActionMailer::Base.deliveries.count).to eq(1)

    expect do
      post :create, {invite: {email: "friend@gmail.com" }}
    end.to_not change{Invite.count}

    expect(ActionMailer::Base.deliveries.count).to eq(1)
    expect(flash[:alert]).to be_present

    advance_invite_time!

    expect do
      post :create, {invite: {email: "friend@gmail.com" }}
    end.to_not change{Invite.count}

    expect(ActionMailer::Base.deliveries.count).to eq(2)

    expect(flash[:notice]).to be_present
  end

  it "registered user can't send invites to another already registered user" do
    friend = create(:user, email: 'friend@gmail.com')

    post :create, {invite: {email: "friend@gmail.com" }}

    expect(ActionMailer::Base.deliveries).to be_empty
    expect(flash[:alert]).to be_present
  end

  it "invite ref should not be changed" do
    post :create, {invite: {email: "friend@gmail.com" }}
    first_email = ActionMailer::Base.deliveries.last

    advance_invite_time!

    post :create, {invite: {email: "friend@gmail.com" }}
    expect(ActionMailer::Base.deliveries.count).to eq(2)
    second_email = ActionMailer::Base.deliveries.last

    first_link = first_email.encoded.match(/href="([^"]+)"/)[1]
    second_link = second_email.encoded.match(/href="([^"]+)"/)[1]

    expect(first_link).to eq(second_link)
  end
end