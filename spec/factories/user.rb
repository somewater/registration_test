FactoryGirl.define do
  factory :user do
    email "registered_user@gmail.com"
    password "password"
    confirmed_at Time.now
  end
end