class Users::RegistrationsController < Devise::RegistrationsController
# before_filter :configure_sign_up_params, only: [:create]
# before_filter :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    if params['confirmation_token'].present? && params['inviter_id'].present?
      cookies[:invite_confirmation_token] = @confirmation_token = params['confirmation_token']
      cookies[:invite_inviter_id] = @inviter_id = params['inviter_id']
    end

    super
  end

  # POST /resource
  def create
    if params[:user][:confirmation_token].present? && params[:user][:inviter_id].present?
      apply_invite(params[:user][:confirmation_token], params[:user][:inviter_id])
    elsif cookies[:invite_confirmation_token].present? && cookies[:invite_inviter_id].present?
      apply_invite(cookies[:invite_confirmation_token], cookies[:invite_inviter_id])
    end

    super
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  def apply_invite(confirmation_token, inviter_id)
    @invite = Invite.find_by(email: params[:user][:email], user_id: inviter_id.to_i,
        confirmation_token: confirmation_token)
    if @invite
      # TODO: apply specific invitation logic like a reward to inviter
    end
  end

  # Override
  def build_resource(hash = nil)
    super(hash)
    if @invite
      resource.confirm
    end
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.for(:sign_up) << :attribute
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.for(:account_update) << :attribute
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
