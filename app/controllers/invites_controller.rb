class InvitesController < ApplicationController

  before_action :authenticate_user!

  def new
    @invite = Invite.new
  end

  def create
    friend_email = params[:invite][:email]
    if User.exists? email: friend_email
      flash.alert = "User with email #{friend_email} already exists"
      return redirect_to new_invites_path
    end

    @invite = Invite.find_or_create_by email: friend_email, user_id: current_user.id
    if @invite.last_invitation_at && (@invite.last_invitation_at +
        Rails.configuration.application_config['invites']['day_timeout'].days).future?
      flash.alert = "You can't send invitation so often"
      return redirect_to new_invites_path
    end

    if @invite.update_column(:last_invitation_at, Time.new)
      invite_link = new_user_registration_url(confirmation_token: @invite.confirmation_token, inviter_id: current_user.id)
      InviteMailer.invite_friend(current_user, invite_link, params[:invite][:email]).deliver
    end

    flash.notice = "Invite to #{friend_email} sent"
    redirect_to new_invites_path
  end

  def confirm

  end
end
