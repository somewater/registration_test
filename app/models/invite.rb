class Invite < ActiveRecord::Base
  before_create :set_confirmation_token

  belongs_to :user

  def set_confirmation_token
    self.confirmation_token = Devise.friendly_token
  end
end
