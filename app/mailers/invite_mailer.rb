class InviteMailer < ApplicationMailer
  def invite_friend user, invite_link, friend_email
    @invite_link = invite_link
    mail(to: friend_email, subject: "User with email #{user.email} want to invite you")
  end
end
